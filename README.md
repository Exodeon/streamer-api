# Streamer API

A toy project made for a technical assignment. This is a RESTFUL API made with Python3 and Flask which interacts with a database of streamers.  
At the moment, we're retrieving data from Twitch API.

## How to run with Docker
⚠️ Make sure that you don't have a SQLite database in your repository, because this will be copied in the container and then the database initialization script will fail.

- Copy `.env.default` as `.env` and fill it in with your Twitch API information (find [here](https://dev.twitch.tv/docs/api/get-started/) how to get it).

- Run the following commands : 
```bash
docker build . -t streamer-api
docker run -p 5000:5000 streamer-api
```

## How to run locally

- Copy `.env.default` as `.env` and fill it in with your Twitch API information (find [here](https://dev.twitch.tv/docs/api/get-started/) how to get it).

- Set up the SQLite database :
```bash
python3 init_database.py
```

- Run the Flask app :
```bash
python3 -m flask --app main run
```

## API Documentation

### Get all streamers

#### Request

`GET /streamers`

#### Response

```json
[
    {
        "platform": "Twitch",
        "profile_picture_url": "https://static-cdn.jtvnw.net/jtv_user_pictures/c5be9ad8-95e9-4937-b4b7-e4dd460a6c36-profile_image-300x300.png",
        "streaming_url": "twitch.tv/mynthos",
        "username": "mynthos"
    },
    {
        "platform": "Twitch",
        "profile_picture_url": "https://static-cdn.jtvnw.net/jtv_user_pictures/7474d9d8-ab27-46c8-95f4-0c71b1878c78-profile_image-300x300.png",
        "streaming_url": "twitch.tv/antoinedaniel",
        "username": "antoinedaniel"
    }
]
```

### Get a streamer

#### Request

`GET /streamers/<username>`

#### Response

```json
{
    "platform": "Twitch",
    "profile_picture_url": "https://static-cdn.jtvnw.net/jtv_user_pictures/c5be9ad8-95e9-4937-b4b7-e4dd460a6c36-profile_image-300x300.png",
    "streaming_url": "twitch.tv/mynthos",
    "username": "mynthos"
}
```

### Add a streamer to database

#### Request

`POST /streamers`

Body :
```json
{
    "username": "zerator",
    "platform": "Twitch"
}
```

#### Response
`201 CREATED`

### Delete a streamer from database

#### Request

`DELETE /streamers/<username>`

#### Response

`204 NO CONTENT`

## Design

### Class diagram

![Class diagram](images/class-diagram.png)