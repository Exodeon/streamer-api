from factory.streaming_platform_mapper_factory import StreamingPlatformMapperFactory
from streaming_platform_connector.twitch_connector import TwitchConnector
from streaming_platform_mapper.twitch_mapper import TwitchMapper


class TwitchMapperFactory(StreamingPlatformMapperFactory):
    def get_streaming_platform_mapper(self) -> TwitchMapper:
        twitch_connector = TwitchConnector()
        twitch_mapper = TwitchMapper(twitch_connector)
        return twitch_mapper
