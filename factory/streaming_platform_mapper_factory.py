from abc import ABC, abstractmethod

from streaming_platform_mapper.streaming_platform_mapper import StreamingPlatformMapper


class StreamingPlatformMapperFactory(ABC):
    @abstractmethod
    def get_streaming_platform_mapper(self) -> StreamingPlatformMapper:
        pass
