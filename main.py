import dataclasses
import logging
import os

from dotenv import load_dotenv
from flask import Flask, request, Response

from database.database_connector import DatabaseConnector
from factory.streaming_platform_mapper_factory import StreamingPlatformMapperFactory
from factory.twitch_mapper_factory import TwitchMapperFactory
from model.streamer import Streamer
from streaming_platform_mapper.streaming_platform_mapper import StreamingPlatformMapper

# load config
load_dotenv()

app = Flask(__name__)
app.logger.setLevel(logging.DEBUG)
database_connector = DatabaseConnector(os.getenv("DATABASE_FILENAME"))

factories = {"Twitch": TwitchMapperFactory()}


def add_new_streamer(platform, username):
    factory: StreamingPlatformMapperFactory = factories[platform]
    streaming_platform_mapper: StreamingPlatformMapper = factory.get_streaming_platform_mapper()
    streamer = streaming_platform_mapper.get_streamer(username)
    database_connector.add_streamer(streamer)


def get_all_streamers():
    list_streamers = database_connector.find_all_streamers()

    # get all streamers from database as a list of tuples, and unpack each of this tuple to create a Streamer object
    list_streamers = list(map(lambda streamer_tuple: Streamer(*streamer_tuple), list_streamers))

    return list_streamers


def get_streamer(username):
    streamer_tuple = database_connector.find_streamer_by_username(username)

    # unpack the tuple to create a Streamer and convert it to dict in order to return a JSON
    return dataclasses.asdict(Streamer(*streamer_tuple))


def delete_streamer(username):
    database_connector.delete_streamer_by_username(username)


@app.route("/streamers", methods=["GET", "POST"])
def streamers():
    # TODO : error handling
    if request.method == "GET":
        return get_all_streamers()
    elif request.method == "POST":
        add_new_streamer(request.json["platform"], request.json["username"])
        return Response(status=201)


@app.route("/streamers/<username>", methods=["GET", "DELETE"])
def streamers_username(username):
    # TODO : error handling
    if request.method == "GET":
        return get_streamer(username)
    elif request.method == "DELETE":
        delete_streamer(username)
        return Response(status=204)
