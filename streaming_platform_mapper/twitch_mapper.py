import logging

from model.streamer import Streamer
from streaming_platform_mapper.streaming_platform_mapper import StreamingPlatformMapper

logger = logging.getLogger("main")


class TwitchMapper(StreamingPlatformMapper):
    def get_streamer(self, username) -> Streamer:
        streamer_json = self.streaming_platform_connector.get_streamer_info(username)["data"][0]
        logger.debug(streamer_json)
        streamer_username = streamer_json["login"]
        streamer_url = "twitch.tv/" + streamer_username
        streamer_profile_picture_url = streamer_json["profile_image_url"]
        return Streamer("Twitch", streamer_username, streamer_url, streamer_profile_picture_url)
