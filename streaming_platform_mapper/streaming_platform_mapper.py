from abc import ABC, abstractmethod

from model.streamer import Streamer


class StreamingPlatformMapper(ABC):
    def __init__(self, streaming_platform_connector):
        self.streaming_platform_connector = streaming_platform_connector

    @abstractmethod
    def get_streamer(self, username) -> Streamer:
        pass
