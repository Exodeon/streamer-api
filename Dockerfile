FROM python:3.10-alpine

RUN python3 -m pip install pip --upgrade
WORKDIR /app
COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY . .

RUN python3 init_database.py

EXPOSE 5000
ENTRYPOINT ["python3", "-m",  "flask", "--app", "main", "run", "--host", "0.0.0.0"]