import sqlite3


class DatabaseConnector:
    def __init__(self, filename):
        self.connection = sqlite3.connect(filename, check_same_thread=False)
        self.cursor = self.connection.cursor()

    def add_streamer(self, streamer):
        self.cursor.execute(f"INSERT INTO streamer VALUES ('{streamer.platform}', "
                                                         f"'{streamer.username}', "
                                                         f"'{streamer.streaming_url}', "
                                                         f"'{streamer.profile_picture_url}')")
        self.connection.commit()

    def find_all_streamers(self):
        streamers = self.cursor.execute("SELECT * FROM streamer")
        return streamers.fetchall()

    def find_streamer_by_username(self, username):
        streamer = self.cursor.execute(f"SELECT * FROM streamer WHERE username = '{username}'")
        return streamer.fetchone()

    def delete_streamer_by_username(self, username):
        self.cursor.execute(f"DELETE FROM streamer WHERE username = '{username}'")
        self.connection.commit()
