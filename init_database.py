import os
import sqlite3

from dotenv import load_dotenv

load_dotenv()

connection = sqlite3.connect(os.getenv("DATABASE_FILENAME"))
cursor = connection.cursor()
cursor.execute("CREATE TABLE streamer(platform, username, streaming_url, profile_picture_url)")
