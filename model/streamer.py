from dataclasses import dataclass


@dataclass
class Streamer:
    platform: str
    username: str
    streaming_url: str
    profile_picture_url: str
