import os

import requests

from streaming_platform_connector.streaming_platform_connector import StreamingPlatformConnector

token_url = "https://id.twitch.tv/oauth2/token"
user_url = "https://api.twitch.tv/helix/users?login="


class TwitchConnector(StreamingPlatformConnector):
    def __init__(self):
        self.client_id = os.getenv("TWITCH_CLIENT_ID")
        self.client_secret = os.getenv("TWITCH_CLIENT_SECRET")
        self.token = self.get_token()

    def get_token(self):
        response = requests.post(token_url,
                                 data=f"client_id={self.client_id}&client_secret={self.client_secret}&grant_type=client_credentials",
                                 headers={"Content-Type": "application/x-www-form-urlencoded"})
        return response.json()["access_token"]

    def get_streamer_info(self, streamer_username):
        response = requests.get(user_url + streamer_username,
                                headers={"Authorization": "Bearer " + self.token,
                                         "Client-Id": self.client_id})
        return response.json()

