from abc import ABC, abstractmethod


class StreamingPlatformConnector(ABC):
    @abstractmethod
    def get_streamer_info(self, streamer_username):
        pass
